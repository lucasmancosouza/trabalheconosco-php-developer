<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

/*
**Method to user register [GET/POST]
**==================================================================
*/

    public function getRegister(){
      return view('register');
    }

    public function postRegister(Request $request){

    //next example will recieve all messages for specific conversation
    $service_url = 'https://somentetop.000webhostapp.com/callback';
    $curl = curl_init($service_url);

    $curl_post_data = array(
      'user_name'   => $request->get('user_name'),
      'user_email'  => $request->get('user_email'),
      'callbackurl' => 'https://xsistemas-challenge-lucas.000webhostapp.com/webhook'
    );
    $curl_post_data = json_encode($curl_post_data);

    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($curl);
    if ($result===false){
      echo(curl_error($curl));
      echo(curl_errno($curl));
    }
    curl_close($curl);
    return(view('register'));

    }


/*
**
** HOOK CONTROLL
** To this function work appropriately the data base need to be configured in the .env file
**
*/

    public function Hook(){

      if($_POST!=[]){
          $info=json_encode($_POST);
      }
      else{
          $info="__BLANK_MESSAGE__";
      }

      DB::insert('insert into CallBacks (info) values ("?")',[$info]);

      return view('webhook');

    }
}

?>
