<!DOCTYPE html>
<html lang="en" dir="ltr">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <head>
    <meta charset="utf-8">
    <title>Register | xSistemas Challange</title>

    <link rel="stylesheet" href="{{asset('/css/stylesheet.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">

  </head>
  <body>

    <section id="conteudo-view" class="register">
      <h1>xSistemas</h1>
      <h3>Challange</h3>
      <p>Entre com usuário e e-mail</p>
      {!! Form::open(['route'=>'user.register','method'=>'post']) !!}
      <label>
          {!! Form::text('user_name',null,['class'=>'input','placeholder'=>"Usuário"])!!}
      </label>
      <label>
          {!! Form::email('user_email',null,['class'=>'input','placeholder'=>"E-mail"])!!}
      </label>
      {!! Form::submit('Entrar')!!}
      {!! Form::close() !!}
    </section>

  </body>
</html>
